package com.imran.assignments.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imran.assignments.R;
import com.imran.assignments.adapter.RecycleAdapter;
import com.imran.assignments.model.ItemModel;
import com.imran.assignments.network.NetworkRequest;
import com.imran.assignments.network.NetworkUpdateListener;
import com.imran.assignments.utils.Constant;
import com.imran.assignments.utils.NetworkUtils;
import com.imran.assignments.utils.RecyclerItemClickListener;
import com.imran.assignments.utils.VolleyUtil;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Imran on 27-12-2015.
 */

public class LandingActivity extends Activity implements NetworkUpdateListener.onUpdateListener {

    private static final String TAG = "LandingActivity" ;
    private RecyclerView mRecyclerView;
    List<ItemModel> mItemsResponse;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView);

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);
      mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                  @Override public void onItemClick(View view, int position) {
                     ItemModel item = mItemsResponse.get(position);
                      Gson gson = new Gson();
                      String itemJson = gson.toJson(item);
                      Intent intent = new Intent(LandingActivity.this,DetailActivity.class);
                      intent.putExtra(Constant.INTENT_DATA_ITEM,itemJson);
                      startActivity(intent);

                  }
              })
      );
        loadDataFromServer();

    }

    private void loadDataFromServer() {
        if (NetworkUtils.isNetworkEnabled(LandingActivity.this)) {
            mProgressBar.setVisibility(View.VISIBLE);
            Type type = new TypeToken<List<ItemModel>>() {}.getType();
            NetworkRequest<ItemModel> request = new NetworkRequest<>(LandingActivity.this, Constant.URL, type, new NetworkUpdateListener(LandingActivity.this, this, Constant.REQUEST_CODE));
            request.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyUtil.getInstance(getApplicationContext()).addToRequestQueue(request, TAG);
        } else {
            Toast.makeText(this, getString(R.string.network_error), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onUpdateView(Object response, boolean success, int reqType) {

        if (success && reqType == Constant.REQUEST_CODE) {
             mItemsResponse = (List<ItemModel>) response;
            mRecyclerView.setAdapter(new RecycleAdapter(LandingActivity.this, mItemsResponse));
        } else {
            Toast.makeText(this, (String) response, Toast.LENGTH_LONG).show();
        }
        mProgressBar.setVisibility(View.GONE);
    }
}
