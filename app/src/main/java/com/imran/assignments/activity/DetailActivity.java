package com.imran.assignments.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.imran.assignments.R;
import com.imran.assignments.model.ItemModel;
import com.imran.assignments.utils.Constant;
import com.imran.assignments.utils.LazyImageLoader;

/**
 * Created by Imran on 27-12-2015.
 */

public class DetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ImageView imageView = (ImageView) findViewById(R.id.image);
        TextView title = (TextView) findViewById(R.id.title);
        TextView description = (TextView) findViewById(R.id.description);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.imageProgressBar);
        String dataJson = getIntent().getStringExtra(Constant.INTENT_DATA_ITEM);
        Gson gson = new Gson();
        ItemModel itemModel = gson.fromJson(dataJson, ItemModel.class);
        title.setText(itemModel.title);
        description.setText(itemModel.description);
//        LazyImageLoader imageLoader = new LazyImageLoader(this);
        LazyImageLoader imageLoader = LazyImageLoader.getInstance(this);
        imageLoader.displayImage(itemModel.image, this, imageView, progressBar);
    }


}
