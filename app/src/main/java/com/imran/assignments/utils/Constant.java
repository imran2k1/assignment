package com.imran.assignments.utils;

/**
 * Created by Imran on 27-12-2015.
 */
public class Constant {
    public static final String URL = "https://gist.githubusercontent.com/maclir/f715d78b49c3b4b3b77f/raw/8854ab2fe4cbe2a5919cea97d71b714ae5a4838d/items.json";
    public static final int REQUEST_CODE = 1;
    public static final String INTENT_DATA_ITEM = "intent_data_item";
}
