package com.imran.assignments.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;


import com.imran.assignments.R;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Stack;

public class LazyImageLoader {
	private final String TAG = LazyImageLoader.class.getSimpleName();
	private static LazyImageLoader mLazyLoader;

	private HashMap<String, Bitmap> cache = new HashMap<String, Bitmap>();
	public static LazyImageLoader getInstance( Context context){
		if (mLazyLoader == null) {
			synchronized (LazyImageLoader.class) {
				if (mLazyLoader == null) {
					mLazyLoader = new LazyImageLoader(context);
				}
			}
		}
	return mLazyLoader;
	}

	private LazyImageLoader(Context context) {
		// Make the background thread low priority.
		photoLoaderThread.setPriority(Thread.NORM_PRIORITY - 3);

	}

	final int defImage_id = R.drawable.image_default;

	public void displayImage(String url, Activity activity, ImageView imageView, ProgressBar progressbar) {
		imageView.setTag(url);
		if (cache.containsKey(url)) {
			progressbar.setVisibility(ProgressBar.INVISIBLE);
			imageView.setVisibility(ImageView.VISIBLE);
			if (null != cache.get(url)) {
				imageView.setImageBitmap(cache.get(url));
			} else {
				imageView.setImageResource(defImage_id);
			}

		} else {
			queuePhoto(url, activity, imageView);
			progressbar.setVisibility(ProgressBar.VISIBLE);
			imageView.setVisibility(ImageView.INVISIBLE);
		}
	}

	private void queuePhoto(String url, Activity activity, ImageView imageView) {
		// This ImageView may be used for other images before. So there may be
		// some old tasks in the queue. We need to discard them.
		photosQueue.Clean(imageView);
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		synchronized (photosQueue.photosToLoad) {
			photosQueue.photosToLoad.push(p);
			photosQueue.photosToLoad.notifyAll();
		}

		// start thread if it's not started yet
		if (photoLoaderThread.getState() == Thread.State.NEW)
			photoLoaderThread.start();
	}

	private Bitmap getBitMap(String url) {
		Bitmap bitmap = null;
		try {
			InputStream is = new URL(url).openStream();
			bitmap = BitmapFactory.decodeStream(is);
		} catch (Exception e) {
			Log.e(TAG, "Exception to download bitmap=" + e.getMessage());
		}

		return bitmap;
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public ImageView imageView;

		public PhotoToLoad(String u, ImageView i) {
			url = u;
			imageView = i;
		}
	}

	PhotosQueue photosQueue = new PhotosQueue();

	public void stopThread() {
		photoLoaderThread.interrupt();
	}

	// stores list of photos to down load
	class PhotosQueue {
		private Stack<PhotoToLoad> photosToLoad = new Stack<PhotoToLoad>();

		// removes all instances of this ImageView
		public void Clean(ImageView image) {
			for (int j = 0; j < photosToLoad.size();) {
				if (photosToLoad.get(j).imageView == image)
					photosToLoad.remove(j);
				else
					++j;
			}
		}
	}

	class PhotosLoader extends Thread {
		public void run() {
			try {
				while (true) {
					// thread waits until there are any images to load in the
					// queue
					if (photosQueue.photosToLoad.size() == 0)
						synchronized (photosQueue.photosToLoad) {
							photosQueue.photosToLoad.wait();
						}
					if (photosQueue.photosToLoad.size() != 0) {
						PhotoToLoad photoToLoad;
						synchronized (photosQueue.photosToLoad) {
							photoToLoad = photosQueue.photosToLoad.pop();
						}

						Bitmap bmp = getBitMap(photoToLoad.url);
						cache.put(photoToLoad.url, bmp);
						Object tag = photoToLoad.imageView.getTag();
						if (tag != null && ((String) tag).equals(photoToLoad.url)) {
							BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad.imageView);
							Activity a = (Activity) photoToLoad.imageView.getContext();
							a.runOnUiThread(bd);
						}
					}
					if (Thread.interrupted())
						break;
				}
			} catch (InterruptedException e) {
				// allow thread to exit
			}
		}
	}

	PhotosLoader photoLoaderThread = new PhotosLoader();

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		ImageView imageView;

		public BitmapDisplayer(Bitmap b, ImageView i) {
			bitmap = b;
			imageView = i;
		}

		public void run() {
			imageView.setVisibility(ImageView.VISIBLE);
			if (bitmap != null) {
				imageView.setImageBitmap(bitmap);
			}
			else {
				imageView.setImageResource(defImage_id);
			}
		}
	}

	public void clearCache() {
		// clear memory cache
		cache.clear();

	}

}
