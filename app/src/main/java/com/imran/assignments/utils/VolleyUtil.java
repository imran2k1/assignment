package com.imran.assignments.utils;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Imran on 27-12-2015.
 */
public class VolleyUtil {
    private static final Object TAG = "VolleyUtil";
    private RequestQueue mRequestQueue;
    private static VolleyUtil mVolleyUtil;
    private static Context mContext;

    private VolleyUtil(){

    }
    public static VolleyUtil  getInstance(Context context){
        mContext = context;
        if (mVolleyUtil == null){
            mVolleyUtil = new VolleyUtil();
        }

        return mVolleyUtil;
    }
    public RequestQueue getRequestQueue() {
        if(mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
