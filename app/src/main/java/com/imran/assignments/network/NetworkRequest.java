package com.imran.assignments.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.imran.assignments.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

/**
 * Created by Imran on 27-12-2015.
 */
public class NetworkRequest<T> extends Request<T> {
    private static final String TAG = "NetworkRequest";

    private Context mContext;
    private Class<T> mClass;
    private Type mType;
    private final NetworkUpdateListener<T> mListener;
    private final Gson mGson = new Gson();

    /**
     * Make a POST request and return a parsed object from JSON.
     *
     * @param url      for request
     * @param tClass   Gson reflection class object
     * @param listener response listener
     */
    public NetworkRequest(Context context, String url, Class<T> tClass, NetworkUpdateListener listener) {
        super(Method.GET, url, listener);
        mContext = context;
        mClass = tClass;
        mListener = listener;
        Log.d(TAG, url);
    }

    public NetworkRequest(Context context, String url, Type type, NetworkUpdateListener listener) {
        super(Method.GET, url, listener);
        mContext = context;
        mType = type;
        mListener = listener;
        Log.d(TAG, url);
    }


    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data, HttpHeaderParser.parseCharset(response.headers));

            if (BuildConfig.DEBUG) Log.d(TAG, "Response :: " + json);

            if (mClass != null) {
                return Response.success(mGson.fromJson(json, mClass)
                        , HttpHeaderParser.parseCacheHeaders(response));
            } else {
                return Response.success((T) mGson.fromJson(json, mType)
                        , HttpHeaderParser.parseCacheHeaders(response));
            }
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return Response.error(new ParseError(ex));
        } catch (JsonSyntaxException ex) {
            ex.printStackTrace();
            return Response.error(new ParseError(ex));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }
}
