package com.imran.assignments.network;

import android.content.Context;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.imran.assignments.R;

/**
 * Created by Imran on 27-12-2015.
 */
public class NetworkUpdateListener  <T> implements Response.Listener<T>, Response.ErrorListener {
    private static final String TAG = NetworkUpdateListener.class.getSimpleName();

    private int mReqType;
    private Context mContext;
    private onUpdateListener mUpdateListener;


    public interface onUpdateListener {
        public void onUpdateView(Object response, boolean success, int reqType);
    }

    public NetworkUpdateListener(Context context, onUpdateListener listener, int reqType) {
        mContext = context;
        mUpdateListener = listener;
        mReqType = reqType;

    }

    @Override
    public void onResponse(T response) {
        if (response == null) {
            mUpdateListener.onUpdateView(response, false, mReqType);
        } else {
            mUpdateListener.onUpdateView(response, true, mReqType);
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String errorMsg;
        if (error instanceof NoConnectionError) {
            errorMsg = mContext.getString(R.string.error_no_network_connection);
        } else if (error instanceof ServerError) {
            errorMsg = mContext.getString(R.string.server_error);
        } else if (error instanceof TimeoutError) {
            errorMsg = mContext.getString(R.string.timeout_error);
        } else {
            errorMsg = mContext.getString(R.string.error_general);
        }
        mUpdateListener.onUpdateView(errorMsg, false, mReqType);
    }

}
