package com.imran.assignments.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.imran.assignments.R;
import com.imran.assignments.model.ItemModel;
import com.imran.assignments.utils.LazyImageLoader;

import java.util.List;

/**
 * Created by Imran on 27-12-2015.
 */
public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ItemHolder> {

    List<ItemModel> mItemModels;
    Activity mActivity;
    LazyImageLoader mLazyImageLoader;

    public RecycleAdapter(Activity activity, List<ItemModel> itemModels){
        mActivity = activity;
        mItemModels = itemModels;
//        mLazyImageLoader = new LazyImageLoader(activity);
        mLazyImageLoader = LazyImageLoader.getInstance(activity);
    }
    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_item, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
            ItemModel modelData = mItemModels.get(position);
            holder.mTitle.setText(modelData.title);
            holder.mDescription.setText(modelData.description);
            mLazyImageLoader.displayImage(modelData.image, mActivity,holder.mImage,holder.mProgressBar);

    }

    @Override
    public int getItemCount() {
        return mItemModels.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        ImageView mImage;
        TextView mTitle, mDescription;
        ProgressBar mProgressBar;

        public ItemHolder(View itemView) {
            super(itemView);
            mImage = (ImageView) itemView.findViewById(R.id.image);
            mTitle = (TextView) itemView.findViewById(R.id.title);
            mDescription = (TextView) itemView.findViewById(R.id.description);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.imageProgressBar);
        }
    }
}
